<?php
if (!function_exists('get_field')) {
    return;
}

$hero_enabled = get_field('hero_enabled');
if (empty($hero_enabled)) {
    return;
}

$hero = get_field('hero');
if (empty($hero)) {
    return;
}

$seo_enabled = isset($hero['seo_enabled']) ? $hero['seo_enabled'] : false;

if (!empty($hero['image']['ID'])) {
    $hero_image = wp_get_attachment_image($hero['image']['ID'], 'original', false, array('class' => 'hero__image objectfit--cover'));
} else {
    /**
     * For the default image, use a srcset generator.
     * @link http://www.responsivebreakpoints.com/
     * Make sure to apply .hero__image class to the img element.
     */
    $hero_image = ''; // default goes here
}

if ( $hero['seo_enabled'] ) {
    $heading = ($hero['heading']) ? '<h1 class="hero__heading">' . $hero['heading'] . '</h1>' : '';
    $subhead = ($hero['subhead']) ? '<h2 class="hero__subhead">' . $hero['subhead'] . '</h2>' : '';
} else {
    $heading = ($hero['heading']) ? '<div class="hero__heading">' . $hero['heading'] . '</div>' : '';
    $subhead = ($hero['subhead']) ? '<div class="hero__subhead">' . $hero['subhead'] . '</div>' : '';
}

?>

<div class="hero hero--<?php echo $hero['size'] ?>">
    <?php echo ($hero_image) ? '<div class="hero__overlay">' . $hero_image .'</div>' : ''; ?>
    <div class="hero__content">
        <div class="container">
            <?php
            echo ($heading) ? $heading : '';
            echo ($subhead) ? $subhead : '';
            echo !empty($hero['link']) ? get_acf_link($hero['link'], 'btn btn--primary hero__link') : '';
            echo !empty($hero['link_secondary']) ? get_acf_link($hero['link_secondary'], 'btn btn--secondary hero__link hero__link--secondary') : '';
            ?>
        </div>
    </div>
</div>
