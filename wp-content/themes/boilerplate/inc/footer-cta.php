<?php
/**
 * CTA
 */

$accumulator = array();

if ( get_field('footer_cta_override_type') == 'suppress' ) {
    return;
} else if ( get_field('footer_cta_override_type') == 'override' ) {
    $accumulator = get_field('footer_cta_repeater');
} else if ( get_field('footer_cta_enabled', 'options') ) {
    $accumulator = get_field('footer_cta_repeater', 'options');
}

$count = count($accumulator);

?>

<?php if ( $count > 0 ) : ?>
    <section class="footer-cta section--footer-cta footer-cta--count-<?php echo $count; ?>">

        <?php
        foreach ( $accumulator as $key => $item ) {
            $item_style = ( $item['image'] ) ? 'style="background-image: url(' . $item['image'] . ');"' : '';
            $item_class = 'footer-cta__item--' . $key;
            $item_class .= ( $item['image'] ) ? ' footer-cta__item--has-img' : '';

            echo '<div class="footer-cta__item ' . $item_class . '" ' . $item_style . '>';
            echo ($count == 1) ? '<div class="container">' : '';

            echo '<div class="footer-cta__inner">';
            echo '<h1 class="footer-cta__heading">';
            echo ( !empty($item['link']) ) ? get_acf_link($item['link'], 'link-passive', $item['heading']) : $item['heading'];
            echo '</h1>';
            echo ( $item['description'] ) ? '<div class="footer-cta__desc">' . $item['description'] . '</div>' : '';
            echo ( !empty($item['link']) ) ? '<div class="footer-cta__btn-group">' : '';
            echo ( !empty($item['link']) ) ? get_acf_link($item['link'], 'btn') : '';
            echo ( !empty($item['link']) ) ? '</div>' : '';
            echo '</div>';

            echo ($count == 1) ? '</div>' : '';
            echo '</div>';
        }
        ?>

    </section>
<?php endif; ?>
