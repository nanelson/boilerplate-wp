<div class="main-search hidden-print">
    <form class="searchform__form" role="search" method="get" action="<?php echo home_url('/'); ?>">
        <div class="searchform__group">
            <input class="searchform__field" type="text" name="s" placeholder="Search" title="Search" value="<?php echo get_search_query() ?>">
            <button class="searchform__btn" type="submit" title="Submit Search"><i class="btb bt-search"></i></button>
        </div>
    </form>
</div>
