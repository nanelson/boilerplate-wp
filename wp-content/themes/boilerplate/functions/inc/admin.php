<?php
/**
 * Admin
 */



/**
 * Remove the admin bar if SHOW_ADMIN_BAR constant is false
 */
if (defined('SHOW_ADMIN_BAR') && !SHOW_ADMIN_BAR) {
    add_filter('show_admin_bar', '__return_false');
}




/**
 * Remove emoji introduced in WP 4.2
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');




/**
 * add page id column to Admin views
 */
add_action('admin_head', 'boilerplate_admin_id_column_style');
function boilerplate_admin_id_column_style()
{
    echo "<style>\n"
       . ".fixed .column-pid { width: 10%; }
          </style>
          ";
}
add_action('admin_init', 'boilerplate_admin_id_column');

function boilerplate_admin_id_column()
{
    // page
    add_filter('manage_pages_columns', 'boilerplate_pid_column');
    add_action('manage_pages_custom_column', 'boilerplate_pid_value', 10, 2);
    // post
    add_filter('manage_posts_columns', 'boilerplate_pid_column');
    add_action('manage_posts_custom_column', 'boilerplate_pid_value', 10, 2);
    // media
    add_filter('manage_media_columns', 'boilerplate_pid_column');
    add_action('manage_media_custom_column', 'boilerplate_pid_value', 10, 2);
    // users
    add_filter('manage_users_columns', 'boilerplate_pid_column');
    add_action('manage_users_custom_column', 'boilerplate_pid_return_value', 10, 3);
    // taxonomy
    foreach(get_taxonomies() as $tax) {
        add_action('manage_edit-' . $tax . '_columns', 'boilerplate_pid_column');
        add_filter('manage_' . $tax . '_custom_column', 'boilerplate_pid_return_value', 10, 3);
    }
}

function boilerplate_pid_column($cols)
{
    $cols['pid'] = 'ID';
    return $cols;
}

function boilerplate_pid_value($column, $id)
{
    if ($column == 'pid') {
        echo $id;
    }
}

function boilerplate_pid_return_value($value, $column, $id)
{
    if ($column == 'pid') {
        $value = $id;
    }
    return $value;
}




/**
 * Flag a development environment with
 * @return void
 */
function boilerplate_staging_flag()
{
    $arr_dev = array(
        'wpengine',
        'localhost',
    );
    foreach ($arr_dev as $dev) {
        if (false !== strpos(home_url(), $dev)) {
            echo '<style> .wp-admin #wpadminbar { background: linear-gradient(to bottom, #d1392a, #d1392a 3px, #23282d 4px, #23282d); } </style>';
            break;
        }
    }
}
add_action('admin_head', 'boilerplate_staging_flag');




/**
 * Modify the widgets on the Admin Dashboard
 * @return void
 */
add_action('wp_dashboard_setup', 'boilerplate_modify_dashboard_widgets');
function boilerplate_modify_dashboard_widgets()
{
    global $wp_meta_boxes;

    // remove WordPress News
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
}
