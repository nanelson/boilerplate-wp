<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
    wp_head();
    ?>

</head>
<body <?php body_class(); ?>>
<?php do_action('bedstone_body_start'); ?>

<div class="body-overlay">

<header class="site-header">
    <div class="container">

        <div class="header-logo">
            <a href="/">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.svg" alt="<?php bloginfo('name'); ?>">
            </a>
            <div class="header-logo__text hidden-text hidden-print"><?php bloginfo('name'); ?></div>
        </div>

        <div class="header-utility hidden-print">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'utility',
                'container' => 'nav',
                'container_class' => 'nav-utility',
                'menu_class' => 'nav-utility__list',
                'depth' => 1,
                'fallback_cb' => false,
            ));
            ?>
        </div>

        <div class="toggle-nav-main">
            <div class="toggle-nav-main__wrap">
                <span class="toggle-nav-main__bar"></span>
                <span class="toggle-nav-main__bar"></span>
                <span class="toggle-nav-main__bar"></span>
            </div>
        </div>

        <nav class="nav-main">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'mobile',
                'container' => '',
                'menu_class' => 'nav-main__list nav-main__list--mobile',
                'depth' => 2,
                'walker' => new Boilerplate_Walker_Nav_Menu(),
                'fallback_cb' => false,
            ));
            wp_nav_menu(array(
                'theme_location' => 'primary',
                'container' => '',
                'menu_class' => 'nav-main__list nav-main__list--primary',
                'depth' => 2,
                'walker' => new Boilerplate_Walker_Nav_Menu(),
                'fallback_cb' => false,
            ));
            ?>

            <div class="social social--header">
                <?php get_template_part('inc/social'); ?>
            </div>

            <div class="header-search hidden-print">
                <form class="searchform__form" role="search" method="get" action="<?php echo home_url('/'); ?>">
                    <div class="searchform__group">
                        <input class="searchform__field" type="text" name="s" placeholder="Search" title="Search" value="<?php echo get_search_query() ?>">
                        <button class="searchform__btn" type="submit" title="Submit Search"><i class="btb bt-search"></i></button>
                    </div>
                </form>
            </div>
        </nav>

    </div>
</header><!-- .site-header -->
