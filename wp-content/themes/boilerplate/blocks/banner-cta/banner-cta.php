<?php

// Support custom "anchor" values.
$anchor = '';
if ( !empty( $block['anchor'] ) ) {
    $anchor = 'id="' . esc_attr( $block['anchor'] ) . '" ';
}

// Create class attribute allowing for custom "className" and "align" values.
$class_name = '';
$class_name .= ( !empty($block['className']) ) ? ' ' . $block['className'] : '';
$class_name .= ( !empty($block['align']) ) ? ' align' . $block['align'] : '';

// Block fields
$img = get_field('b_banner_cta_image');
$heading = get_field('b_banner_cta_heading');
$link = get_field('b_banner_cta_link');

?>

<section <?php echo $anchor; ?> class="block-banner-cta <?php $class_name; ?>">
    <div class="container">
        <?php
        echo ($heading) ? '<h2 class="banner-cta__heading">' . $heading . '</h2>' : '';
        echo (!empty($link)) ? get_acf_link($link, 'btn banner-cta__btn') : '';
        ?>
    </div>

    <?php
    if ( !empty($img) ) {
        echo '<div class="banner-cta__img">';
        echo wp_get_attachment_image($img['id'], 'original', false, array('class' => 'objectfit--cover'));
        echo '</div>';
    }
    ?>
</section>