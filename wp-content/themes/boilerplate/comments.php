<?php
if (post_password_required() || (!comments_open() && 0 == get_comments_number())) {
	return;
}
?>

<section class="comments">
    <header class="comments__header">
        <h1 class="comments__heading">Comments</h1>
    </header>

	<?php if (have_comments()) : ?>

	    <div class="comments__list">
            <?php wp_list_comments(array('style' => 'div', 'short_ping' => true)); ?>
	    </div>

		<?php if (1 < get_comment_pages_count() && get_option('page_comments')) : ?>
		    <footer class="comments__footer">
        		<nav class="nav-comments">
        			<div class="nav-comments--prev"><?php previous_comments_link('Older Comments'); ?></div>
        			<div class="nav-comments--next"><?php next_comments_link('Newer Comments'); ?></div>
        		</nav>
    		</footer>
		<?php endif; ?>

	<?php endif; ?>

    <?php if (comments_open()) : ?>
    	<div class="comments__form hidden-print">
    	    <?php
            comment_form(array(
                'class_submit' => 'btn',
                'comment_notes_before' => '',
                'format' => 'html5',
                'title_reply' => 'Submit a Comment',
            ));
            ?>
    	</div>
    <?php endif; ?>
</section>
