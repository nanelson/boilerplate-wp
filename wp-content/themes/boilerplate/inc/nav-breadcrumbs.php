<?php
/**
 * breadcrumb nav
 *
 * @package Bedstone
 */

$breadcrumbs = boilerplate_get_breadcrumbs();
?>

<?php if (!empty($breadcrumbs)) : ?>
    <nav class="nav-breadcrumbs hidden-print">
        <ul class="nav-breadcrumbs__list">
            <?php
            foreach ($breadcrumbs as $crumb) {
                echo '<li class="breadcrumbs__item">';
                echo !empty($crumb['link']) ? '<a href="' . $crumb['link'] . '">': '<span>';
                echo $crumb['name'];
                echo !empty($crumb['link']) ? '</a>' : '</span>';
                echo '</li>';
            }
            ?>
        </ul>
    </nav>
<?php endif; ?>
