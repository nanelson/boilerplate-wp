<?php
/**
 * default content output
 * page
 * single
 * attachment
 *
 * @package Bedstone
 */
?>

<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">

    <section class="content clearfix">
        <?php the_content(); ?>
    </section>

    <?php comments_template(); ?>

</div>
