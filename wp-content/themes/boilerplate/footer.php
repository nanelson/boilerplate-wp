<footer class="site-footer">
    <?php get_template_part('inc/footer-cta'); ?>

    <div class="footer-primary">
        <div class="container">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'footer',
                'container' => 'nav',
                'container_class' => 'nav-footer hidden-print',
                'menu_class' => 'nav-footer__list',
                'depth' => 1,
                'fallback_cb' => false,
            ));
            ?>
        </div>
    </div>

    <div class="footer-secondary">
        <div class="container">

            <div class="copyright">
                &copy; <?php echo date('Y') . ' ' . get_bloginfo('name'); ?>
            </div>

            <?php
            wp_nav_menu(array(
                'theme_location' => 'legal',
                'container' => 'nav',
                'container_class' => 'nav-legal hidden-print',
                'menu_class' => 'nav-legal__list',
                'depth' => 1,
                'fallback_cb' => false,
            ));
            ?>

        </div>
    </div>

</footer>

</div><!-- .body-overlay -->

<?php wp_footer(); ?>

</body>
</html>
