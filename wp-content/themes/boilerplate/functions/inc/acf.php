<?php
/**
 * ACF things
 */



function enable_acf_options_page()
{
    if (function_exists('acf_add_options_page')) {
        acf_add_options_page();
        acf_add_options_sub_page('Footer CTA');
        acf_add_options_sub_page('Contact Information');
    }
}
add_action('init', 'enable_acf_options_page');



function my_acf_init()
{
    acf_update_setting('google_api_key', GOOGLE_MAPS_KEY);
}
add_action('acf/init', 'my_acf_init');



/**
 * ACF link field type
 * @param  array $arr_link Link attributes from ACF
 * @param  string $class Class name
 * @param  string $title Link label
 * @return string
 */
function get_acf_link($arr_link, $class = '', $title = '')
{
    $ret = '';
    if (!empty($arr_link)) {
        $link_target = (!empty($arr_link['target'])) ? ' target="' . $arr_link['target'] . '" ' : '';
        $link_html = '<a class="' . $class . '" href="' . $arr_link['url'] . '" ' . $link_target . '>';
        if ('' == $title) {
            $link_html .= $arr_link['title'];
        } else {
            $link_html .= $title;
        }
        $link_html .= '</a>';
        $ret = $link_html;
    }
    return $ret;
}




/**
 * ACF Remove standard custom fields meta box
 */
if (function_exists('get_field')) {
    add_filter('acf/settings/remove_wp_meta_box', '__return_true');
}
