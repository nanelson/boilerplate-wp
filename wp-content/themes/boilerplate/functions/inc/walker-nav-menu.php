<?php
/**
 * @see /wp-includes/class-walker-nav-menu.php
 */
class Boilerplate_Walker_Nav_Menu extends Walker_Nav_Menu
{
    public function start_lvl( &$output, $depth = 0, $args = array() )
    {
        $output .= '<div class="nav-main__submenu">';
        parent::start_lvl($output, $depth, $args);
    }

    public function end_lvl( &$output, $depth = 0, $args = array() )
    {
        parent::end_lvl($output, $depth, $args);
        $output .= '</div>';
    }

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
    {
        $item->classes = empty( $item->classes ) ? array() : (array) $item->classes;
        if (0 == $depth) {
            $item->classes[] = 'nav-main__item'; // top level
            if ($args->walker->has_children) {
                $item->classes[] = 'nav-main__item--parent'; // is toggle
            }
            if (!empty($item->object) && 'megamenu' == $item->object) {
                $item->classes[] = 'nav-main__item--parent'; // is toggle
                $item->classes[] = 'nav-main__item--parent--mega'; // is mega
            }
        }
        parent::start_el($output, $item, $depth, $args, $id);
    }
}



function boilerplate_walker_nav_menu_start_el($item_output, $item, $depth, $args)
{
    if (!empty($item->object) && 'megamenu' == $item->object) {
        global $post;
        $post = get_post($item->object_id);
        ob_start();
        get_template_part('inc/megamenu');
        $megamenu = ob_get_clean();
        wp_reset_postdata();
        if (!empty($megamenu)) {
            $item_output .= $megamenu;
        }
    }
    return $item_output;
}
add_filter('walker_nav_menu_start_el', 'boilerplate_walker_nav_menu_start_el', 10, 4);
