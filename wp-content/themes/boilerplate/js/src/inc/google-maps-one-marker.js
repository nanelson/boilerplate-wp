;(function($) {
    // Creating a "plugin" that creates Google Maps with optional Single Marker
    // call using a class for multiple maps, make sure the called element has a unique ID
    $.fn.googleMapsOneMarker = function( options ) {

        // Establish our default settings
        var settings = $.extend({
            styles: null,
            zoom: 14,
            disableDefaultUI: true,
            zoomControl: true,
            scrollwheel: false,
            center: {lat: 0, lng: 0},
            hasMarker: false,
            marker: null
        }, options);

        return this.each( function() {
            var theID = $(this).attr('id'),
                theElData = $(this).data(),
                mapName = 'map_' + theID;

            // Set Initial Map options
            mapOptions = {
                styles: settings.styles,
                zoom: settings.zoom,
                disableDefaultUI: settings.disableDefaultUI,
                zoomControl: settings.zoomControl,
                scrollwheel: settings.scrollwheel,
                center: settings.center,
                hasMarker: settings.hasMarker,
                marker: settings.marker
            };

            // Override Map Options with $element data
            if ( theElData.zoom ) {
                mapOptions.zoom = theElData.zoom;
            }
            if ( theElData.lat && theElData.lng ) {
                mapOptions.center = {lat: theElData.lat, lng: theElData.lng};
            }
            if ( theElData.hasMarker != null ) {
                mapOptions.hasMarker = theElData.hasMarker;
            }

            // Creat the Map
            mapName = new google.maps.Map( document.getElementById(theID), mapOptions);

            // If has marker
            if ( mapOptions.hasMarker ) {
                // Create a marker for each place.
                markerOptions = {
                    map: mapName,
                    position: mapOptions.center
                };

                // Custom Marker
                if (mapOptions.marker) {
                    markerOptions.icon = {
                        url: mapOptions.marker.url,
                        size: new google.maps.Size( mapOptions.marker.size[0], mapOptions.marker.size[1] ),
                        origin: new google.maps.Point( 0, 0 ),
                        anchor: new google.maps.Point( mapOptions.marker.size[0] / 2, mapOptions.marker.size[1]),
                        scaledSize: new google.maps.Size( mapOptions.marker.size[0], mapOptions.marker.size[1] ),
                    };

                    // override defaults
                    if ( mapOptions.marker.origin ) {
                        markerOptions.icon.origin = new google.maps.Point( mapOptions.marker.origin[0], mapOptions.marker.origin[1])
                    }
                    if ( mapOptions.marker.anchor ) {
                        markerOptions.icon.anchor = new google.maps.Point( mapOptions.marker.anchor[0], mapOptions.marker.anchor[1])
                    }
                }

                var marker = new google.maps.Marker(markerOptions);
            }
        });

    }
}(jQuery));
