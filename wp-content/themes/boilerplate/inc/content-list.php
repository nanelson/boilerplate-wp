<?php
/**
 * content output, list
 *
 * @package Bedstone
 */

// determine if we should use post thumbnail
$has_thumb = (!is_search() && has_post_thumbnail()) ? true : false;
?>

<article <?php post_class('list-article clearfix ' . ($has_thumb ? 'list-article--has-thumb' : 'list-article--no-thumb')); ?> id="post-<?php the_ID(); ?>">

    <?php if ($has_thumb) : ?>
        <a href="<?php the_permalink(); ?>">
            <div class="list-article__thumb" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
        </a>
    <?php endif; ?>

    <div class="list-article__content">
        <header class="article-header">
            <h1 class="article-header__title style-h2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
            <?php
            if (is_singular(array('post'))) {
                get_template_part('inc/nav-article-meta');
            }
            ?>
        </header>

        <section class="list-article__desc content">
            <?php the_excerpt(); ?>
        </section>

        <a class="cta-link" href="<?php echo get_the_permalink(); ?>">Read More</a>
    </div>

</article>
