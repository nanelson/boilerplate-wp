<?php
/**
 * content output, empty, e.g. no search results
 *
 * @package Bedstone
 */
?>

<article>
    <div class="list-article__content">
        <header class="article-header">
            <h1 class="article-header__title">No items found</h1>
        </header>
        <section class="content">
            <p class="callout">We're sorry &ndash; we could not find any items that match your request.</p>
            <p class="call-to-action"><a href="/">Visit Our Homepage</a></p>
        </section>
    </div>
</article>
