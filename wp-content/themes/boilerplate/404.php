<?php
get_header();
?>

<div class="site-columns">
    <div class="container">

        <div class="columns">
            <main class="site-main col col-lg-8">
                <?php get_template_part('inc/site-headline'); ?>
                <div class="content">
                    <p class="callout">We're sorry &ndash; we could not find the page you requested.</p>
                    <p class="call-to-action"><a href="/">Visit Our Homepage</a></p>
                </div>
                <?php get_search_form(); ?>
            </main>
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>

<?php
get_footer();
