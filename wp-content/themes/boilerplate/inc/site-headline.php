<?php
/**
 * site-wide include
 *
 * @package boilerplate
 */
?>
<header class="site-headline">
    <?php get_template_part('nav/breadcrumbs'); ?>

    <h1 class="headline-title">
        <?php
        if (is_page() || is_single()) {
            boilerplate_the_alternate_title();
        } elseif (is_404()) {
            echo 'Page Not Found';
        } elseif (is_search()) {
            echo 'Results for: ' . get_search_query();
        } elseif (is_category()) {
            single_cat_title();
        } elseif (is_tag()) {
            single_tag_title();
        } elseif (is_tax()) {
            $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
            echo $term->name;
        } elseif (is_author()) {
            echo 'Author: ' . get_the_author();
        } elseif (is_day()) {
            echo 'Archive: ' . get_the_date('l, F j, Y');
        } elseif (is_month()) {
            echo 'Archive: ' . get_the_date('j Y');
        } elseif (is_year()) {
            echo 'Archive: ' . get_the_date('Y');
        } elseif (is_home() && is_front_page()) {
            // Settings > Reading > Front Page Displays > Your Latest Posts
            echo get_bloginfo('name');
        } elseif (is_home()) {
            // Settings > Reading > Front Page Displays > Static Page > Posts Page
            echo get_the_title(get_option('page_for_posts', true));
        } elseif (is_archive()) {
            post_type_archive_title();
        } else {
            echo 'Archives';
        }
        ?>
    </h1>

    <?php
    if (is_singular(array('post'))) {
        get_template_part('inc/nav-article-meta');
        get_template_part('inc/social-share');
    }
    ?>

</header>
