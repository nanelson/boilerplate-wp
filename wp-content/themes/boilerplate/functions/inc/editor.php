<?php
/**
 * wysiwyg functions
 */


function add_style_formats($init)
{
    $style_formats = array(
        array(
            'title' => 'Lead',
            'selector' => 'p',
            'classes' => 'lead',
        ),
        array(
            'title' => 'Callout',
            'selector' => 'p',
            'classes' => 'callout',
        ),
        array(
            'title' => 'Call-to-Action',
            'selector' => 'p',
            'classes' => 'call-to-action',
        ),
        array(
            'title' => 'Footnote',
            'selector' => 'p',
            'classes' => 'footnote',
        )
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init['style_formats'] = json_encode($style_formats);
    return $init;
}
// add_filter('tiny_mce_before_init', 'add_style_formats');




/**
 * Custom mce editor styles
 */
add_action('init', 'custom_editor_style');
function custom_editor_style()
{
    add_editor_style('css/dst/' . STYLE_EDITOR);
}




/**
 * Enqueue Gutenberg block editor style
 */
if ( has_action('enqueue_block_editor_assets') ) {
    add_action( 'enqueue_block_editor_assets', 'boilerplate_gutenberg_editor_styles' );
    function boilerplate_gutenberg_editor_styles() {
        wp_enqueue_style( 'boilerplate-block-editor-styles', get_theme_file_uri('css/' . STYLE_EDITOR), false, CACHE_BUSTER, 'all' );
    }
}




/**
 * Custom MCE editor blockformats
 *     ** this should come BEFORE any other MCE-related functions
 */
add_filter('tiny_mce_before_init', 'boilerplate_editor_items');
function boilerplate_editor_items($init)
{
    // Add block format elements you want to show in dropdown
    $init['block_formats'] = 'Paragraph=p; Heading (h1)=h1; Heading (h2)=h2; Sub-heading (h3)=h3';
    // Disable unnecessary items and buttons
    $init['toolbar1'] = 'bold,italic,alignleft,aligncenter,alignright,bullist,numlist,link,unlink,hr,table'; // 'template,|,bold,italic,strikethrough,bullist,numlist,blockquote,hr,alignleft,aligncenter,alignright,link,unlink,wp_more,spellchecker,wp_fullscreen,wp_adv',
    $init['toolbar2'] = 'formatselect,pastetext,removeformat,charmap,undo,redo,wp_help,styleselect'; // 'formatselect,underline,alignjustify,forecolor,pastetext,removeformat,charmap,outdent,indent,undo,redo,wp_help',
    // Display the kitchen sink by default
    $init['wordpress_adv_hidden'] = false;
    // [optional] Add elements not included in standard tinyMCE dropdown
    //$init['extended_valid_elements'] = 'code[*]';
    return $init;
}




/**
 * Advanced Custom Fields: Custom MCE editor blockformats
 */
add_filter('acf/fields/wysiwyg/toolbars', 'boilerplate_acf_editor_items');
function boilerplate_acf_editor_items($toolbars)
{
    if (isset($toolbars['Full'][1])) {
        $toolbars['Full'][1] = array('bold', 'italic', 'alignleft', 'aligncenter', 'alignright', 'bullist', 'numlist', 'link', 'unlink', 'hr', 'table');
    }

    if (isset($toolbars['Full'][2])) {
        $toolbars['Full'][2] = array('formatselect', 'pastetext', 'removeformat', 'charmap', 'undo', 'redo', 'wp_help', 'styleselect');
    }

    return $toolbars;
}
