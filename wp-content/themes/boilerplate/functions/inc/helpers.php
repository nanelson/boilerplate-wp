<?php
/**
 * Development helpers
 */



/**
 * Retrive alternate title, if available, to be used for SEO or display purposes
 *
 * @return string Title
 */
function boilerplate_get_the_alternate_title($id = null)
{
    global $post;
    if (!$id && is_object($post)) {
        $id = $post->ID;
    }

    if (function_exists('get_field')) {
        $alternate_title = get_field('alternate_title', $id);
    }

    if (!empty($alternate_title)) {
        $ret = $alternate_title;
    } else {
        $ret = get_the_title($id);
    }

    return $ret;
}

function boilerplate_the_alternate_title($id = null)
{
    echo boilerplate_get_the_alternate_title($id);
}




/**
 * Get the root parent
 * @param int $id Post id
 * @return int Post id of the root-most parent
 */
function boilerplate_get_the_root_parent($id = false)
{
    $root = 0;
    if (!$id) {
        global $post;
        $id = isset($post->ID) ? $post->ID : 0;
    }
    $ancestors = get_post_ancestors($id);
    if (!empty($ancestors)) {
        $root = end($ancestors);
    } else {
        $root = $id;
    }
    return $root;
}

function boilerplate_the_root_parent($id = false)
{
    echo boilerplate_get_the_root_parent($id);
}




/**
 * Get breadcrumbs array
 * @param  boolean $show_front       Should the front page (e.g. "Home") be used? T/F
 * @param  boolean $show_single_item Should we return breadcrumbs that have only one item? T/F
 * @return array                     Breadcrumb name, link values
 */
function boilerplate_get_breadcrumbs($show_front = true, $show_single_item = true)
{
    $breadcrumbs = array();
    $page_on_front = get_option('page_on_front');
    $page_for_posts = get_option('page_for_posts', true);

    // initialize with front page
    if ($show_front && $page_on_front && !is_front_page()) {
        $breadcrumbs[] = array(
            'name' => get_the_title($page_on_front),
            'link' => get_permalink($page_on_front),
        );
    }

    if ($page_for_posts && is_home()) {
        $breadcrumbs[] = array('name' => get_the_title($page_for_posts));
    } elseif (is_404()) {
        $breadcrumbs[] = array('name' => 'Page Not Found');
    } elseif (is_search()) {
        $breadcrumbs[] = array('name' => 'Search Results');
    } elseif ($page_for_posts && !is_tax() && (is_single() || is_archive())) {
        $breadcrumbs[] = array(
            'name' => get_the_title($page_for_posts),
            'link' => get_permalink($page_for_posts),
        );
        if (is_category()) {
            $breadcrumbs[] = array('name' => single_cat_title('', false));
        } elseif (is_tag()) {
            $breadcrumbs[] = array('name' => single_tag_title('', false));
        }
    } elseif (!is_front_page()) {
        $ancestors = get_ancestors(get_the_ID(), 'page');
        $ancestors = array_reverse($ancestors);
        foreach ($ancestors as $ancestor) {
            $breadcrumbs[] = array(
                'name' => get_the_title($ancestor),
                'link' => get_permalink($ancestor),
            );
        }
        $breadcrumbs[] = array('name' => get_the_title());
    }
    if (!$show_single_item && 1 == count($breadcrumbs)) {
        $breadcrumbs = array();
    }
    return $breadcrumbs;
}




/**
 * Filter title for trademarks
 * Replaces reg and tm with html superscript element and html chars
 */
if (!is_admin()) {
    // does not filter in the admin area
    add_filter('the_title', 'boilerplate_title_trademarks');
}
function boilerplate_title_trademarks($title)
{
    $title = str_replace('&copy;', '<sup>&copy;</sup>', $title);
    $title = preg_replace('/\x{00A9}/u', '<sup>&copy;</sup>', $title);
    $title = str_replace('&reg;', '<sup>&reg;</sup>', $title);
    $title = preg_replace('/\x{00AE}/u', '<sup>&reg;</sup>', $title);
    $title = str_replace('&trade;', '<sup>&trade;</sup>', $title);
    $title = preg_replace('/\x{2122}/u', '<sup>&trade;</sup>', $title);
    $title = str_replace('&#8480;', '<sup>&#8480;</sup>', $title); // service mark
    $title = preg_replace('/\x{2120}/u', '<sup>&#8480;</sup>', $title); // service mark
    return $title;
}


/**
 * Filter title for permissions labels
 */
function boilerplate_permissions_title_format($format)
{
    return '%s';
}
add_filter('private_title_format', 'boilerplate_permissions_title_format' );
add_filter('protected_title_format', 'boilerplate_permissions_title_format');




/**
 * Custom Post Type Helpers
 * @link https://bitbucket.org/snippets/windmilldesign/KBxe6/custom-post-types
 */
function custom_post_type_labels($singular, $plural = null, $labels = array())
{
    if(!$plural) $plural = $singular.'s';

    $defaults = array(
        'name'                      => $plural,
        'singular_name'             => $singular,
        'menu_name'                 => $plural,
        'name_admin_bar'            => $singular,
        'all_items'                 => 'All '.$plural,
        'add_new'                   => 'Add New ',
        'add_new_item'              => 'Add New ',
        'edit_item'                 => 'Edit '.$singular,
        'new_item'                  => 'New '.$singular,
        'view_item'                 => 'View '.$singular,
        'search_items'              => 'Search '.$plural,
        'not_found'                 => 'No '.$plural.' found',
        'not_found_in_trash'        => 'No '.$plural.' found in Trash',
        'parent_item_colon'         => 'Parent '.$plural.':'
    );

    return array_merge($defaults, $labels);
}




function custom_taxonomy_labels($singular, $plural = null, $labels = array())
{
    if(!$plural) $plural = $singular.'s';

    $defaults = array(
        'name'                       => $plural,
        'singular_name'              => $singular,
        'menu_name'                  => $plural,
        'all_items'                  => 'All '.$plural,
        'edit_item'                  => 'Edit '.$singular,
        'view_item'                  => 'View '.$singular,
        'update_item'                => 'Update '.$singular,
        'add_new_item'               => 'Add New '.$singular,
        'new_item_name'              => 'New '.$singular,
        'parent_item'                => 'Parent '.$singular,
        'parent_item_colon'          => 'Parent '.$singular.':',
        'search_items'               => 'Search '.$plural,
        'popular_items'              => 'Popular '.$plural,
        'separate_items_with_commas' => 'Separate '.strtolower($plural).' with commas',
        'add_or_remove_items'        => 'Add or remove '.strtolower($plural),
        'choose_from_most_used'      => 'Choose from the most used '.strtolower($plural),
        'not_found'                  => 'No '.$plural.' found'
    );

    return array_merge($defaults, $labels);
}




