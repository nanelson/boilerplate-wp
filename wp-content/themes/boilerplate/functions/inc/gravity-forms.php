<?php
/**
 * Gravity Forms
 */



/**
 * Allow visibility setting for input labels
 */
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );



/**
 * Submit input to Button
 */
function input_to_button( $button, $form ) {
    $button_custom_class = 'btn'; // SET THIS HERE
    $dom = new DOMDocument();
    $dom->loadHTML( $button );
    $input = $dom->getElementsByTagName( 'input' )->item(0);
    $new_button = $dom->createElement( 'button' );
    $new_span = $dom->createElement( 'span' );
    $new_button->appendChild( $new_span );
    $new_span->appendChild( $dom->createTextNode( $input->getAttribute( 'value' ) ) );
    $input->removeAttribute( 'value' );
    foreach( $input->attributes as $attribute ) {
        $new_button->setAttribute( $attribute->name, $attribute->value );
    }
    if ($new_button->hasAttribute('class')) {
        $new_button->setAttribute('class', $new_button->getAttribute('class') . ' ' . $button_custom_class);
    } else {
        $new_button->setAttribute('class', $button_custom_class);
    }
    $input->parentNode->replaceChild( $new_button, $input );
    return $dom->saveHtml( $new_button );
}
add_filter( 'gform_next_button', 'input_to_button', 10, 2 );
add_filter( 'gform_previous_button', 'input_to_button', 10, 2 );
add_filter( 'gform_submit_button', 'input_to_button', 10, 2 );
