<?php
function cpt_megamenu()
{
    register_post_type('megamenu', array(
        'labels' => custom_post_type_labels('Mega Menu'),
        'public' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title', 'custom-fields'),
        'rewrite' => false,
    ));
}
// add_action('init', 'cpt_megamenu', 10, 0);
