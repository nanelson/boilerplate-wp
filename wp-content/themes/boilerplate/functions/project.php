<?php
require 'inc/helpers.php';
require 'inc/admin.php';
require 'inc/enqueue.php';
require 'inc/updates.php';
require 'inc/analytics.php';
require 'inc/acf.php';
require 'inc/gravity-forms.php';
require 'inc/editor.php';
require 'inc/walker-nav-menu.php';
require 'inc/megamenu.php';



function custom_theme_setup()
{
    add_theme_support('title-tag');
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');
    add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption'));
    add_post_type_support('page', array('excerpt'));
    register_nav_menus(array(
        'primary' => 'Primary: 2 levels',
        'mobile' => 'Mobile: 2 levels',
        'utility' => 'Utility: 1 level',
        'footer' => 'Footer: 1 level',
        'legal' => 'Legal: 1 level',
    ));
}
add_action('after_setup_theme', 'custom_theme_setup');




function modify_excerpt_length($length)
{
    return 25; // number of words
}
//add_filter('excerpt_length', 'modify_excerpt_length');




/**
 * Filter body class
 */
add_filter('body_class', 'boilerplate_body_class');
function boilerplate_body_class($classes)
{
    $root_parent = false;
    if (is_front_page()) {
        $root_parent = 'front-page';
    } elseif (is_home()) {
        $root_parent = 'home';
    } elseif (is_category()) {
        $root_parent = 'category';
    } elseif (is_tag()) {
        $root_parent = 'tag';
    } elseif (is_author()) {
        $root_parent = 'author';
    } elseif (is_day() || is_month() || is_year()) {
        $root_parent = 'date';
    } elseif (is_search()) {
        $root_parent = 'search';
    } elseif (is_singular(array('post'))) {
        $root_parent = 'post';
    } elseif (is_singular(array('page'))) {
        $root_parent = boilerplate_get_the_root_parent();
    }
    if ($root_parent) {
        $classes[] = 'root-parent-' . $root_parent;
    }
    return $classes;
}




/**
 * @link http://codex.wordpress.org/Content_Width
 */
if (!isset($content_width)) {
    $content_width = 640; // in pixels
}
