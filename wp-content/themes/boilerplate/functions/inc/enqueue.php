<?php
/**
 * Enqueu
 */


function custom_enqueue_scripts()
{
    $cache_buster = defined('CACHE_BUSTER') ? CACHE_BUSTER : 'missing-cache-buster-constant';

    // style
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans');
    // wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '4.7.0');
    wp_enqueue_style('black-tie', get_template_directory_uri() . '/black-tie/css/black-tie.min.css');
    wp_enqueue_style('boilerplate-css', get_template_directory_uri() . '/css/dst/app.css', array(), $cache_buster);

    // script
    wp_enqueue_script('google-maps-js', 'https://maps.googleapis.com/maps/api/js?v=3&key=' . GOOGLE_MAPS_KEY, array(), 'NoVersion', true);
    wp_enqueue_script('app-js', get_template_directory_uri() . '/js/dst/app-min.js', array('jquery'), $cache_buster, true);
}
add_action('wp_enqueue_scripts', 'custom_enqueue_scripts');


// add_filter("script_loader_tag", "add_module_to_my_script", 10, 3);
// function add_module_to_my_script($tag, $handle, $src)
// {
//     if ( "app-js" === $handle ) {
//         $tag = '<script type="module" src="' . esc_url($src) . '"></script>';
//     }

//     return $tag;
// }