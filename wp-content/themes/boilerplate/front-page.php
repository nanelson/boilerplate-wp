<?php
get_header();
get_template_part('inc/hero');
?>

<div class="container">
    <main class="site-main">

        <?php
        while (have_posts()) {
            the_post();
            get_template_part('inc/content');
        }
        ?>

    </main>
</div>

<?php
get_footer();
