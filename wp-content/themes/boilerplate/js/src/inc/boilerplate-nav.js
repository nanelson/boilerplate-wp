function boilerplateNavPlain(){
    const navMain = document.querySelector('.nav-main');
    const topLevels = navMain.querySelectorAll('.nav-main__item');
    const navMainLinks = navMain.querySelectorAll('.nav-main__item a');
    const subMenus = navMain.querySelectorAll('.nav-main__item__submenu');
    const navToggle = document.querySelector('.toggle-nav-main');
    let winWidth = window.innerWidth;
    let timeout_resize = null;

    navMainLinks.forEach(el => {
        el.addEventListener('click', function(e){
            
            e.preventDefault();
            const thisClosestSubmenu = e.target.closest('.nav-main__item__submenu');

            if ( !thisClosestSubmenu ) {
                const thisTopLevel = e.target.closest('.nav-main__item');
                const thisSubmenu = thisTopLevel.querySelector('.nav-main__item__submenu');

                if ( thisSubmenu ) {
                    e.preventDefault();

                    if (!document.body.classList.contains('nav-main--active')) {
                        // mobile nav is not active or is desktop view
                        topLevels.forEach(el => {
                            if ( el != thisTopLevel ) {
                                el.classList.remove('nav-main__item--active');
                            }
                        });
                    } else {
                        // mobile nav is active
                        nnSlideToggle(thisSubmenu, 300);
                    }

                    thisTopLevel.classList.toggle('nav-main__item--active');
                }
            }
        });
    });

    navToggle.addEventListener('click', function(e){
        e.preventDefault();
        document.body.classList.toggle('nav-main--active');
    });

    document.addEventListener('click', function(e){
        if ( !e.target.closest('.nav-main__item') ) {
            topLevels.forEach(el => el.classList.remove('nav-main__item--active'));
        }
    });

    function doResetNav(){
        if ( winWidth != window.innerWidth ) {
            document.body.classList.remove('nav-main--active');
            topLevels.forEach(el => el.classList.remove('nav-main__item--active'));
            subMenus.forEach(el => el.style.display = '');
            winWidth = window.innerWidth;
        }
    }

    window.addEventListener('resize', function(){
        window.clearTimeout(timeout_resize);
        timeout_resize = window.setTimeout(doResetNav, 200);
    });
}
boilerplateNavPlain();
