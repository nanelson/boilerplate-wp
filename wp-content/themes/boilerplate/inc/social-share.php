<?php
$title = urlencode(get_the_title());
$blogname = urlencode(get_bloginfo('name'));
$link = urlencode(get_permalink());
$tweet = urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'));
$raw_title = rawurlencode(get_the_title());
$raw_blogname = rawurlencode(get_bloginfo('name'));
$raw_link = rawurlencode(get_permalink());

$facebook = 'https://www.facebook.com/sharer/sharer.php?u=' . $link;
$twitter = 'https://twitter.com/intent/tweet?url=' . $link . '&text=' . $tweet;
$pinterest = 'https://www.pinterest.com/pin/create/button/';
$linkedin = 'https://www.linkedin.com/shareArticle?mini=true&amp;url=' . $link . '&amp;title=' . $title . '&amp;summary=&amp;source=';
$email = 'mailto:?subject=' . $raw_blogname . rawurlencode(': ') . $raw_title . '&amp;body=' . $raw_title . rawurlencode("\r\n") . $raw_link;
?>

<div class="social-share hidden-print">
    <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
    <span class="social-share__label">Share:</span>
    <ul class="social__list">
        <li class="social__item">
            <a class="social__link" rel="external" href="<?php echo $facebook; ?>"><i class="fab fab-facebook-alt"></i></a>
        </li>
        <li class="social__item">
            <a class="social__link" rel="external" href="<?php echo $twitter; ?>"><i class="fab fab-twitter"></i></a>
        </li>
        <li class="social__item">
            <a class="social__link" rel="external" href="<?php echo $linkedin; ?>"><i class="fab fab-linkedin-alt"></i></a>
        </li>
        <li class="social__item">
            <a class="social__link" data-pin-do="buttonBookmark" data-pin-custom="true" href="<?php echo $pinterest; ?>"><i class="fab fab-pinterest-alt"></i></a>
        </li>
        <li class="social__item">
            <a class="social__link" rel="external" href="<?php echo $email; ?>"><i class="bts bt-envelope"></i></a>
        </li>
    </ul>
</div>
