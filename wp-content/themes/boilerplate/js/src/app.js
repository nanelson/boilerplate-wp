// @codekit-prepend quiet "vendor/modernizr-custom.js";
// @codekit-prepend quiet "inc/reframe.js";
// @codekit-prepend "inc/helpers.js";
// @codekit-prepend "inc/boilerplate-nav.js";



// ======================================================================
// Fitvids
// ======================================================================
elsContentVideoPlayers = document.querySelectorAll('.content iframe[src*="youtube.com"], .content iframe[src*="vimeo.com"]');
reframe(elsContentVideoPlayers);



// ======================================================================
// Content Tables
// ======================================================================
elsContentTables = document.querySelectorAll('.content table');

if ( elsContentTables ) {

    elsContentTables.forEach(el => {
        const wrapper = document.createElement('div');

        // create wrapper container
        el.parentNode.insertBefore(wrapper, el);

        // insert wrapper before el in the DOM tree
        el.parentNode.insertBefore(wrapper, el);

        // move el into wrapper
        wrapper.appendChild(el);
    });
}