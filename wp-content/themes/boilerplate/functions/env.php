<?php
$env_debug = false;
$env = array();
$host = (!empty($_SERVER['X_FORWARDED_HOST'])) ? $_SERVER['X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];

$hosts = array(
    'STAGING'     => 'example.wpengine.com',
);

// set default values for the PRODUCTION environments
$env['ENV_SHOW_ANALYTICS'] = true;

if (in_array($host, $hosts) || false !== strpos('wpengine', $host)) {
    // set values for NON-PRODUCTION environments
    $env['ENV_SHOW_ANALYTICS'] = false;

    if (!empty($hosts['STAGING']) && $host == $hosts['STAGING']) {
        // set env-specific values
    }
}

if (file_exists(__DIR__ . '/env-local.php')) {
    if (!require(__DIR__ . '/env-local.php')) {
        exit('When processing $env settings, there was an error trying to include the local settings file.');
    }
}

if (!empty($env)) {
    foreach ($env as $name => $value) {
        if (!define($name, $value)) {
            exit("When processing \$env settings, the define() of '$name' failed.");
        }
    }
}

if ($env_debug) {
    $lf = "\n\n";
    echo "View Source... $lf <!-- $lf \$host $lf";
    var_dump($host);
    echo "$lf \$env $lf";
    ksort($env);
    var_dump($env);
    echo "$lf get_defined_constants()['user'] $lf";
    $defined_constants = array_intersect_key(get_defined_constants(true), array('user' => 1));
    ksort($defined_constants['user']);
    var_dump($defined_constants['user']);
    echo "$lf -->";
    exit;
}

unset(
    $env_debug,
    $env,
    $host,
    $hosts
);
