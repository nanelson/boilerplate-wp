<?php

/*
 * Whitelist specific Gutenberg blocks (paragraph, heading, image and lists)
 *
 * @author Misha Rudrastyh
 * @link https://rudrastyh.com/gutenberg/remove-default-blocks.html#allowed_block_types_all
 */

add_filter( 'allowed_block_types_all', 'nn_allowed_block_types', 25, 2 );
function nn_allowed_block_types( $allowed_blocks, $editor_context ) {

    return array(
        'core/image',
        'core/paragraph',
        'core/heading',
        'core/list',
        'core/list-item',
        'core/quote',
        'core/embed',
        'nn/banner-cta',
    );

}




add_action( 'init', 'register_custom_blocks' );
function register_custom_blocks() {
	register_block_type( __DIR__ . '/banner-cta/block.json' );
    // CSS - use "style": "file:./banner-cta.css" in block.json? - howver it loads both on front-end and back-end
}