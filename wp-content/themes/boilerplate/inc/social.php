<ul class="social__list">
    <li class="social__item">
        <a class="social__link" rel="external" href="#"><i class="fab fab-facebook-alt"></i></a>
    </li>
    <li class="social__item">
        <a class="social__link" rel="external" href="#"><i class="fab fab-twitter"></i></a>
    </li>
    <li class="social__item">
        <a class="social__link" rel="external" href="#"><i class="fab fab-linkedin-alt"></i></a>
    </li>
</ul>
