<aside class="sidebar col col-lg-4">

    <?php if (is_home() || is_single() || is_category() || is_tag() || is_date()) : ?>
        <nav class="nav-categories sidebar-item hidden-print">
            <h4 class="sidebar-item__heading nav-categories__heading">Categories</h4>
            <ul class="sidebar-item__list nav-categories__list">
                <?php wp_list_categories('title_li='); ?>
            </ul>
        </nav>
    <?php endif; ?>

</aside>
