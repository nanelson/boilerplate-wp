<?php
/**
 * default for archives -- e.g. author, category, cpt, ctax, date, search, tag
 */
get_header();
?>

<div class="site-columns">
    <div class="container">

        <div class="columns">
            <main class="site-main col col-lg-8">
                <?php
                get_template_part('inc/site-headline');
                if (is_search()) {
                    get_search_form();
                }
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        get_template_part('inc/content-list');
                    }
                    get_template_part('inc/nav-archive');
                } else {
                    get_template_part('inc/content-none');
                }
                ?>
            </main>
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>

<?php
get_footer();
