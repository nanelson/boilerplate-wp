<?php
get_header();
get_template_part('inc/hero');
?>

<div class="site-columns">
    <div class="container">

        <div class="columns">
            <main class="site-main col col-lg-8">
                <?php
                get_template_part('inc/site-headline');
                the_post();
                get_template_part('inc/content');
                if (is_singular(array('post'))) {
                    get_template_part('inc/nav-posts');
                }
                ?>
            </main>
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>

<?php
get_footer();
